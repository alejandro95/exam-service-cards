FROM maven:3.6-jdk-8-alpine AS builder
VOLUME /tmp
ADD ./target/exam-service-cards-0.0.1-SNAPSHOT.jar service-cards.jar
ENTRYPOINT ["java","-jar","/service-cards.jar"]