package com.finalexam.trujillo.app.cards.controller;


import com.finalexam.trujillo.app.cards.model.response.CardResponse;
import com.finalexam.trujillo.app.cards.service.ICardService;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/core")
public class CardController {

    @Autowired
    private ICardService iCardService;


    @GetMapping("/cards")
    public Single<CardResponse> obtainCard(@RequestParam String documentNumber){

        return iCardService.obtainCard(documentNumber);
    }



}
