package com.finalexam.trujillo.app.cards.model.response;


import com.finalexam.trujillo.app.cards.model.entity.Card;
import lombok.*;

import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CardResponse {

    private List<Card> cards;

}
