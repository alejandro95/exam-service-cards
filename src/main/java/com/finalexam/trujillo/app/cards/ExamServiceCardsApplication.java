package com.finalexam.trujillo.app.cards;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamServiceCardsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamServiceCardsApplication.class, args);
    }

}
