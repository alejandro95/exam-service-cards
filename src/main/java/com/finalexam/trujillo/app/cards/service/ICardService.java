package com.finalexam.trujillo.app.cards.service;


import com.finalexam.trujillo.app.cards.model.response.CardResponse;
import io.reactivex.Single;

public interface ICardService {

    Single<CardResponse> obtainCard(String document);

}
