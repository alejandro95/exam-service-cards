package com.finalexam.trujillo.app.cards.service;

import com.finalexam.trujillo.app.cards.model.entity.Card;
import com.finalexam.trujillo.app.cards.model.response.CardResponse;
import io.reactivex.Single;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class CardServiceImpl implements ICardService{


    @Override
    public Single<CardResponse> obtainCard(String document) {
        return Single.just(CardResponse.builder()
                .cards(Arrays.asList(Card.builder()
                        .cardNumber("1111222233334441")
                        .active(true)
                        .build(),
                        Card.builder()
                                .cardNumber("1111222233334442")
                                .active(true)
                                .build(),
                        Card.builder()
                                .cardNumber("1111222233334443")
                                .active(true)
                                .build(),
                        Card.builder()
                                .cardNumber("1111222233334444")
                                .active(false)
                                .build(),
                        Card.builder()
                                .cardNumber("1111222233334445")
                                .active(false)
                                .build(),
                        Card.builder()
                                .cardNumber("1111222233334446")
                                .active(false)
                                .build()
                        ))
                .build());
    }
}
