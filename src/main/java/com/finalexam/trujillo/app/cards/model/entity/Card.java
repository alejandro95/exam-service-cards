package com.finalexam.trujillo.app.cards.model.entity;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Card {

    private String cardNumber;
    private Boolean active;
}
